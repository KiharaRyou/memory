const fetch = require('../utils/request');
const _ = require('lodash');

class OxfordDict {
  constructor({ url, app_id, app_key, sourceLang } = {}) {
    if (!url) throw new Error('url fail');
    this.url = url;
    if (!app_id) throw new Error('app id fail');
    if (!app_key) throw new Error('app key fail');
    this.headers = { app_id, app_key };
    this.sourceLang = sourceLang || 'en';
  }
  formatDefinitions(Entry, Thesaurus) {
    const thesaurusSenses = [];
    if(!Thesaurus.error) {
      Thesaurus.results[0].lexicalEntries.forEach(item => {
        item.entries.forEach(entry => {
          entry.senses.forEach(sense => thesaurusSenses.push(sense));
        });
      });
    }
    let results = [];
    Entry.results[0].lexicalEntries.forEach(item => {
      const entries = [];
      item.entries.forEach(entry => {
        entry.senses.forEach(sense => {
          const thesaurus = [];
          if(sense.thesaurusLinks && !Thesaurus.error) {
            sense.thesaurusLinks.forEach(link => {
              const relatedWords = _.find(thesaurusSenses, o => o.id === link.sense_id);
              if(relatedWords)thesaurus.push(relatedWords);
            });
          }
          entries.push({
            thesaurus,
            definitions: sense.definitions,
            examples: sense.examples
          });
        });
      }); 
      results.push({
        lexicalCategory: item.lexicalCategory,
        pronunciation: _.find(item.pronunciations, o => o.audioFile !== undefined),
        entries
      });
    });  
    return results;
  }

  async getInflection(word) {
    const opts = {
      headers: {...this.headers}
    }
    const res = await fetch(`${this.url}/inflections/${this.sourceLang}/${word}`, opts);
    return res; 
  }

  async getThesaurus(word) {
    const opts = {
      headers: {...this.headers}
    }
    const res = await fetch(`${this.url}/entries/${this.sourceLang}/${word}/synonyms;antonyms`, opts);
    return res; 
  }

  async getEntry(word, region) {
    if(!region) {
      region = 'us';
    }
    const opts = {
      headers: {...this.headers}
    }
    const res = await fetch(`${this.url}/entries/${this.sourceLang}/${word}/regions=${region}`, opts);
    return res; 
  }
}

module.exports = OxfordDict;