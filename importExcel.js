const fs = require('fs');
const XLSX = require('xlsx');
const models = require('./models');

const importExcel = () => {
  const buf = fs.readFileSync('./public/word_frequency.xlsx');
  const wb = XLSX.read(buf, {type: 'buffer'});
  const json = XLSX.utils.sheet_to_json(wb.Sheets.Sheet1);
  json.forEach(e => {
  	const entry = {
  		text: e.text.trim(),
  		active: true
  	}
  	models.Entry.create(entry)
  	  .catch(err => console.log(err));
  })
  // let count = json.length;
  // setInterval(()=> {
  // 	const entry = {
  // 		text: json[count - 1].text.trim(),
  // 		active: true
  // 	}
  // 	models.Entry.create(entry)
  // 	  .catch(err => console.log(err))
  // 	count++;	
  // 	if(count === 0) {
  // 	  clearInterval()
  // 	}
  // }, 1000)
}

module.exports = importExcel;