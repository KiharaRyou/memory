const fs = require('fs');
const _ = require('lodash');
const Sequelize = require('sequelize');
const moment = require('moment');
const download = require('../utils/download');
const formData = require('../utils/formdata');
const TelegramBot = require('../telegram');
const telegramConfig = require('../config/telegram');
const botId = telegramConfig.botId;
const tg = new TelegramBot({botId});

const OxfordDict = require('../OxfordDict');
const oxfordDictConfig = require('../config/oxfordDict');
const models = require('../models');

const randomNumber = require('../utils/randomNumber');
const ox = new OxfordDict({...oxfordDictConfig});
const Op = Sequelize.Op;

exports.processUpdate = async update => {
  console.log(update);
  const { message, callback_query } = update;
  if(message) {
    this.processMessage(update);
  }
  if(callback_query) {
    this.processCallbackQuery(update);
  }
}
exports.processMessage = async update => {
  const { text, from } = update.message;
  const { User } = models;
  let user;
  if(text.startsWith('/')) {
    const command = text.split(' ');
    switch(command[0]) {
      case '/start':
	user = await this.activeUser(from.id).catch(err => console.log(err));
	const cards = await user.getCards();
	if(!cards.length > 0) {
	  this.addNewWord(user);
	}
	break;
      case '/ox_dict':
	if(!command[1]) {
	  tg.sendMessage(from.id, 'parameter is required, e.g: /oxDict example')
            .catch(err => console.log(err));
	  return;
	}
	this.sendOxDefinitions(from.id, command[1]).catch(err => console.log(err));
	break;	
      case '/ox_update_entry':
	if(!command[1]) {
	  tg.sendMessage(from.id, 'parameter is required, e.g: /oxUpdateEntry example')
            .catch(err => console.log(err));
	  return;
	}
	const result = await this.addEntry(command[1], true).catch(err => console.log(err));
	if(!result.error) {
	  this.sendOxDefinitions(from.id, command[1]).catch(err => console.log(err));
	}
	break;
      case '/ox_add_entry':
        user = await User.find({
          where: {
            userId: from.id
          }
        }).catch(err => console.log(err));
        let res;
        if(command[1]) {
	  res = this.createCard(user, {card: command[1], type: 1});
	} else {
          res = this.addNewWord(user);
        }
        if(res.error) {
          tg.sendMessage(from.id, res.message)
            .catch(err => console.log(err));
        }
        break;
      case '/ox_audio':
        if(!command[1]) {
          tg.sendMessage(from.id, 'parameter is required, e.g: /oxAudio example')
                  .catch(err => console.log(err));
          return;
        }
        this.sendOxAudio(from.id, command[1]).catch(err => console.log(err));
        break;
      case '/next_card':
        this.sendNextCard(from.id).catch(err => console.log(err));
        break;
      case '/change_mode':
        const updateResult = this.updateUser(from.id, {mode: command[1] ? command[1] : 0}).catch(err => console.log(err));
        if(updateResult.error) {
          tg.sendMessage(from.id, updateResult.message)
                  .catch(err => console.log(err));
        } else {
          tg.sendMessage(from.id, `Successful change`)
                  .catch(err => console.log(err));
        }
        break;  
      default:
	return;
    }
  }
}
exports.processCallbackQuery = async update => {
  const { User, Card } = models;
  const { data, from, message } = update.callback_query;
  const user = await User.find({
    where: {
      userId: from.id
    }
  }).catch(err => console.log(err));
  const clearBtns = async () => {
    await tg.updateMessage('editMessageReplyMarkup', message.chat.id, message.message_id, {reply_markup:{}});
    if(user.mode === 1) {
      this.sendNextCard(from.id);
    }
  }	
  const card = await Card.find({
    where: {
      currentCard: message.message_id
    }
  }).catch(err => console.log(err));
  const callback = data.split(' ');
  switch(callback[0]) {
    case 'memorize_card_success':
      await user.update({status: 1});
      const diff = moment(card.next_date).diff(card.last_date, 'days');
      const next_date = moment().add(diff + 1, 'days');
      await card.update({
        next_date: next_date.format('YYYY/MM/DD HH:mm:ss'),
        last_date: new Date(),
        currentCard: null
      });
      await clearBtns();
      break;
    case 'memorize_card_fail':
      await user.update({status: 1});
      await card.update({
        next_date: moment().add(12, 'hours').format('YYYY/MM/DD HH:mm:ss'),
        last_date: new Date(),
        currentCard: null
      });
      await clearBtns();
      break;
    case 'get_definitions':
      this.sendOxDefinitions(from.id, callback[1] ? callback[1] : card.card)
          .catch(err => console.log(err));
      break;	
    case 'disable_card':
      await user.update({status: 1});
      await card.update({
        active: false,
      }).catch(err => console.log(err));
      await clearBtns();
      break;
    case 'get_ox_audio':
      this.sendOxAudio(from.id, callback[1]);
      break;  
    default:
      return;
  }
}

exports.addEntry = async (text, update) => {
  const { Entry } = models;
  const record = await Entry.find({
    where: {
      text
    }
  }).catch(err => console.log(err));
  if(record && record.inflection && !update) {
    return record;
  } else {
    const inflection = await ox.getInflection(text).catch(err => console.log(err));
    const entry = await ox.getEntry(text).catch(err => console.log(err));
    if(!inflection.error || !entry.error) {
      const thesaurus = await ox.getThesaurus(text).catch(err => console.log(err));
      if(!record) {
	return Entry.create({
	  text,
	  entry: JSON.stringify(entry),
	  inflection: JSON.stringify(inflection),
	  thesaurus: JSON.stringify(thesaurus),
	  active: true
	})
      } else {
	return record.update({
	  entry: JSON.stringify(entry),
	  inflection: JSON.stringify(inflection),
	  thesaurus: JSON.stringify(thesaurus),
	});
      }
    } else {
      return {message: 'No such entry found', ...inflection};
    }
  }
}
exports.addNewWord = async (user) => {
  const { Entry } = models;
  const cards = await user.getCards();
  const texts = cards.map(card => card.card);
  const entries = await Entry.findAll({
    where: {
      text: {
	[Op.notIn]: texts
      },
      active: true
    }
  }).catch(err => console.log(err));
  const len = entries.length;
  if(entries && len > 0) {
    card = entries[randomNumber(0, len - 1)].text;
    return this.createCard(user, {card, type: 1});
  }
}

exports.createCard = async (user, options) => {
  const { Card } = models;
  const cards = await user.getCards().catch(err => console.log(err));
  const texts = cards.map(card => card.card);
  if(options.card) {
    const index = _.indexOf(texts, options.card);
    if(index !== -1) {
      return {error: true, message: 'Card already existed'};
    } 
  } else {
    return {error: true, message: 'Card name is required'};
  }
  const today = new Date();
  const defaultOpts = {
    UserId: user.userId,
    next_date: today,
    last_date: today,
    active: true,
  }
  if(options.type === 1) {
    const entry = await this.addEntry(options.card).catch(err => console.log(err));
    if(!entry.error) {
      const card = await Card.create({
        ...defaultOpts,
        ...options
      }).catch(err => console.log(err)); 
      this.sendOxCard(user, card);
    } else {
      tg.sendMessage(user.userId, entry.error);
    }
    	
  } 
}

exports.activeUser = async userId => {
  const { User } = models;
  const user = await User.find({
    where: {
      userId
    }
  }).catch(err => console.log(err));
  if(user) {
    if(user.status !== 1) {
      return user.update({status: 1});
    } else {
      return user;
    }
  } else {
    return User.create({ userId, status: true});
  }
}

exports.updateUser = async (userId, data) => {
  const { User } = models;
  const user = await User.find({
    where: {
      userId
    }
  }).catch(err => console.log(err));
  if(user) {
    return user.update(data);
  } else {
    return {error: true, message: 'User does not exist'};
  }
}

// exports.setCard = async (userId) => {
//   const { User, Card } = models;
// 	const user = await User.find({
// 		where: {
// 			userId
// 		}
// 	}).catch(err => console.log(err));
// 	if(user) {
// 		let today = new Date();
// 		today.setHours(23,59,59,999);
// 		const card = await Card.find({where: {
// 			UserId: userId,
// 			next: {
// 			  [Op.lte]: today
// 			}
// 		}}).catch(err => console.log(err));
// 		if(card) {
// 			this.updateUser(userId, {status: 2});
// 			return user.setCurrentCard(card);
// 		} else {
// 			return 'Did not has any card'
// 		}
// 	} else {
// 		return 'User does not exist';
// 	}	
// }

exports.getWebhookInfo = async () => {
  const result = await tg.getWebhookInfo()
                         .catch(error => {
                           console.log(error);
                           return error;
	                 });
  return result;
}

exports.setWebhook = async () => {
  const result = await tg.setWebhook()
                         .catch(error => {
                           console.log(error);
                           return error;
	                 });
  return result;
}

exports.sendOxCard = async (user, card) => {
  const definitions = await this.getOxDefinitions(card.card).catch(err=>console.log(err));
  if(definitions.error) {
    tg.sendMessage(user.userId, definitions.message)
      .catch(err => console.log(err));
      return;
  }
  const opts = {
    parse_mode: 'markdown',
    reply_markup: {
      inline_keyboard: [
        [{
          text: '⭕️',
          callback_data: 'memorize_card_success'
        }, {
          text: '❌',
          callback_data: 'memorize_card_fail'
        }],
        [{
          text: '📖definitions',
          callback_data: 'get_definitions'
        },{
          text: '🚫disable',
          callback_data: 'disable_card'
        }],
      ]
    }
  }
  if(definitions.audios && definitions.audios.length > 0) {
    opts.reply_markup.inline_keyboard.push([{
      text: `🔊 `, callback_data: `get_ox_audio ${card.card}`
    }]);
  }
  if(user.status === 1) {
    let text = '🔰' + card.card + 
	       '\n------------------------------------------------- \n';
    const res = await tg.sendMessage(user.userId, text, opts).catch(err => console.log(err));			
    if(res.ok) {
      await user.update({status: 2}).catch(err => console.log(err));
      await card.update({currentCard: res.result.message_id}).catch(err => console.log(err));;
    }
  }
}

exports.sendOxDefinitions = async (id, text) => {
  const definitions = await this.getOxDefinitions(text);
  if(definitions.error) {
    tg.sendMessage(id, definitions.message)
      .catch(err => console.log(err));
  } else {
    const opts = {reply_markup: {inline_keyboard:[]}, parse_mode: 'markdown'};
    if(definitions.type === 'entry' && definitions.audios.length > 0) {
      opts.reply_markup.inline_keyboard.push([{
          text: `🔊 `, callback_data: `get_ox_audio ${text}`
        }]);
    }
    if(definitions.type === 'inflection') {
      const row = [];
      definitions.inflections.forEach(inflection =>
        row.push({
          text: `See ${inflection}`, callback_data: `get_definitions ${inflection}`
        })
      );
      opts.reply_markup.inline_keyboard.push(row);
    }
    tg.sendMessage(id, definitions.content, opts).catch(err => console.log(err));
  }
}
exports.sendOxAudio = async (chat_id, text) => {
  const { Audio } = models;
  const definitions = await this.getOxDefinitions(text);
  if(!definitions.error) {
    if(definitions.audios.length > 0) {
      definitions.audios.forEach(async audio => {
        const oxAudio = await Audio.find({
          where: {
            type: 1,
            remark: audio.audioFile
          }
        }).catch(err => console.log(err));
        if(oxAudio) {
          tg.sendFormData('sendAudio', formData({
            chat_id,
            audio: oxAudio.file_id,
            performer: 'Oxford Dictionaries'
          }))
        } else {
          const dlResult = await download(audio.audioFile)
            .catch(err => console.log(err)); 
          if(dlResult === 'finish') {
            const urlSplit = audio.audioFile.split('/');
            const sendResult = await tg.sendFormData('sendAudio', formData({
              chat_id,
              audio: fs.createReadStream(`${__appdir}/downloads/${urlSplit[urlSplit.length - 1]}`),
              performer: 'Oxford Dictionaries'
            }))
                                       .catch(err => console.log(err)) 
            Audio.create({
              file_id: sendResult.result.audio.file_id,
              type: 1,
              remark: audio.audioFile
            });
          }
        }
      });
    } else {
      tg.sendMessage(id, 'Audio not founed')
        .catch(err => console.log(err));
    }
  } else {
    tg.sendMessage(id, definitions.message)
      .catch(err => console.log(err));
  }
}
exports.sendNextCard = async userId => {
  const { User, Card } = models;
  const user = await User.find({
    where: {
      userId
    }
  }).catch(err => console.log(err));
  let currentTime = moment();
  const currentHour = parseFloat(currentTime.format('HH'));
  if(currentHour < 12 ) {
    currentTime.set({hour: 11, minute: 59, second: 59, millisecond: 999});
  } else {
    currentTime.set({hour: 23, minute: 59, second: 59, millisecond: 999});
  }
  const cards = await Card.findAndCountAll({
    where: {
      UserId: userId,
      active: true,
      next_date: {
        [Op.lte]: currentTime, 
      },
    },
    order:['next_date']
  }).catch(err => console.log(err))
  if(cards.count > 0) {
    const card = cards.rows[0];
    this.sendOxCard(user, card);	
  } else {
    this.addNewWord(user);
  }
}

exports.kintokiSchedule = async () => {
  const { User, Card } = models;
  const users = await User.findAll({where: {
    userId: {[Op.ne]: 'admin'},
    status: 1,
    mode: 0
  }}).catch(err => console.log(err));
  if(users) {
    users.forEach(async user => {
      this.sendNextCard(user.userId);
    })
  }
}

exports.getOxDefinitions = async text => {
  const { Entry } = models;
  const record = await Entry.find({
    where: {
      text
    }
  }).catch(err => console.log(err));
  let data;
  if(record && record.inflection) {
    data = record;
  } else {
    data = await this.addEntry(text).catch(err => console.log(err));
  }
  if(data.error) {
    return data;
  }
  const entry = JSON.parse(data.entry);
  if(!entry.error) {
    const results = ox.formatDefinitions(entry, JSON.parse(data.thesaurus));
    let definitions = '📚*Oxford Dictionaries* \n \n' +
		      '*' + text + '*' +
	              '\n  ------------------------------------------------- \n';
    const audios = [];
    results.forEach(definition => {
      const { lexicalCategory, entries, pronunciation } = definition;
      definitions += `\n*${lexicalCategory}* \n`;
      if(pronunciation) {
        if(pronunciation.audioFile && _.find(audios, o => o.audioFile === pronunciation.audioFile) === undefined) {
          audios.push(pronunciation);
        }
	definitions += `/${pronunciation.phoneticSpelling}/ \n`;	
      }
      entries.forEach(entry => {
	const { thesaurus } = entry;
	if(entry.definitions) {
	  definitions += `\n🔖${entry.definitions.join('/')} \n`;
	}
	if(entry.examples) {
	  definitions += '_examples:_ \n';
	  entry.examples.forEach((example,index) => definitions += `    _${index + 1}.${example.text}_ \n`);
	}
	if(thesaurus) {
	  thesaurus.forEach(item => {
	    if(item.synonyms) {
	      definitions += '\n synonyms: \n';
	      item.synonyms.forEach(synonym => definitions += ` ${synonym.text}; `);
	    }
	    if(item.antonyms) {
	      definitions += '\n antonyms: \n';
	      item.antonyms.forEach(antonym => definitions += ` ${antonym.text}; `);
	    }
	  });
	}
	definitions += `\n`;
      });
      definitions += `\n`;
    });
    definitions += '\n------------------------------------------------- \n';
    return {type: 'entry', content: definitions, audios};
  } else {
    let content = '📚*Oxford Dictionaries* \n \n' +
		  '*' + text + '*' +
	          '\n------------------------------------------------- \n';
    const inflections = [];
    JSON.parse(data.inflection).results[0].lexicalEntries.forEach(item => {
      const { lexicalCategory, grammaticalFeatures, inflectionOf } = item;
      content += `\n*${lexicalCategory}* \n`;
      if(grammaticalFeatures) {
	content += '\nGrammatical Features: \n'
	grammaticalFeatures.forEach((feature, index) => {
	  content += `_${index + 1}. ${feature.text} ${feature.type}_ \n`
	});
      }
      if(inflectionOf) {
	content += 'Inflection of: \n';
	inflectionOf.forEach(inflection => {
	  content += `${inflection.text}; `
	  if(inflections.indexOf(inflection.id) === -1)inflections.push(inflection.id);
	});
	content += `\n`;
      }
      content += `\n`;
    });
    content += '\n  ------------------------------------------------- \n';		
    return {type: 'inflection', content, inflections};
  }
}
