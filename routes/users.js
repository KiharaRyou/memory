const express = require('express');
// const redis = require('redis');
const models = require('../models');
const TelegramBot = require('../telegram');
const telegramConfig = require('../config/telegram');

const router = express.Router();
// const redisClient = redis.createClient();
const botId = telegramConfig.botId;
const authorId = telegramConfig.authorId;
const tg = new TelegramBot({botId});

router.get('/',(req,res) => {
  tg.getWebhookInfo()
    .then(response => console.log(response))
    .catch(error => console.log(error))
});

module.exports = router;
