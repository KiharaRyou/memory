const express = require('express');
const kintoki = require('../controllers/kintoki');
const router = express.Router();

const { getWebhookInfo, setWebhook, processUpdate, addEntry, getOxDefinitions } = kintoki;

router.get('/',(req,res) => {
  res.send('Here is Kintoki telegram bot');
});

router.post('/',async (req,res) => {
  res.send('200');
  processUpdate(req.body);
});

router.get('/getWebhookInfo', async (req,res) => {
  const result = await getWebhookInfo()
    .catch(error => {
      console.log(error);
      res.send('error');
    });
  res.send(result);
});

router.post('/setWebhook', async (req,res) => {
  const result = await setWebhook(req.body)
    .catch(error => {
      console.log(error);
      res.send('error');
    });
  res.send(result);
});

module.exports = router;