module.exports = (sequelize, DataTypes) => {
  var Card = sequelize.define('Card', {
    next_date: DataTypes.DATE,
    last_date: DataTypes.DATE,
    active: DataTypes.BOOLEAN,
    card: {type: DataTypes.TEXT, allowNull: false},
    description: DataTypes.TEXT,
    telegram_audio_file_id: DataTypes.STRING(200),
    type: DataTypes.TINYINT,
    currentCard: DataTypes.STRING(20),
  });
  return Card;
};