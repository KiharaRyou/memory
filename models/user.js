module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    userId: {type: DataTypes.STRING(20), unique: true, allowNull: false, primaryKey: true},
    status: DataTypes.TINYINT,
    mode: DataTypes.TINYINT
  });
  User.associate = (models) => {
    models.User.hasMany(models.Card, { foreignKey: 'UserId' });
  };
  return User;
};
