module.exports = (sequelize, DataTypes) => {
  var Audio = sequelize.define('Audio', {
    file_id: DataTypes.STRING(200),
    type: DataTypes.TINYINT,
    remark: DataTypes.TEXT
  });
  return Audio;
};