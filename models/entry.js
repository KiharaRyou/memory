module.exports = (sequelize, DataTypes) => {
  var Entry = sequelize.define('Entry', {
    text: {type: DataTypes.STRING(50), unique: true, allowNull: false, primaryKey: true},
    entry: DataTypes.TEXT,
    inflection: DataTypes.TEXT,
    thesaurus: DataTypes.TEXT,
    active: DataTypes.BOOLEAN
  });
  return Entry;
};