const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const dotenv = require('dotenv');
const flash = require('express-flash');
const path = require('path');
const expressValidator = require('express-validator');
const expressStatusMonitor = require('express-status-monitor');
const useragent = require('express-useragent');
const multer = require('multer');
const upload = multer({ dest: path.join(__dirname, 'uploads') });
const models = require('./models');
const importExcel = require('./importExcel');
const kintokiSchedule = require('./controllers/kintoki').kintokiSchedule;
const kintoki = require('./routes/kintoki');
const users = require('./routes/users');
const schedule = require('node-schedule');

global.__appdir = __dirname;
/**
* Load environment variables from .env file, where API keys and passwords are configured.
 */
//dotenv.load({ path: '.env.example' });


/**
 * Create Express server.
 */
const app = express();

const kintokiCorn = schedule.scheduleJob('*/5 * * * *', () => {
	kintokiSchedule();
});


/**
 * Express configuration.
 */
app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 6079);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(expressStatusMonitor());
app.use(useragent.express());
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({type: '*/xml'}));
app.use(expressValidator());
app.use(session({
  secret: 'verysecret',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 }
}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));



/**
* route
 */
app.use('/kintoki', kintoki);
app.use('/users', users);

/**
* Error Handler.
 */
app.use(errorHandler());

/**
* Start Express server.
 */

models.sequelize.sync().then((err) => {
  // importExcel()
  //if(err){
    //throw err[0];
  //}else{
  models.User.find({where:{userId:'admin'}}).then(user => {

      if(!user){
  models.User.create({userId: 'admin', status: true});
      }
    })
  /**
   * Listen on provided port, on all network interfaces.
   */
  app.listen(app.get('port'), () => {
    console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
    console.log('  Press CTRL-C to stop\n');
  });
  //}
  
});



module.exports = app;
