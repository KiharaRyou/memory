const fetch = require('../utils/request');

class Telegram {
  constructor({ botId } = {}) {
    if (!botId) throw new Error('botId fail');
    this.botId = botId;
    this.url = `https://api.telegram.org/bot${botId}`;
  }

  async getWebhookInfo() {
    const res = await fetch(`${this.url}/getWebhookInfo`);
    return res;
  }

  async setWebhook(data) {
    const opts = {
      method: 'POST',
      body: { ...data },
    }  
    const res = await fetch(`${this.url}/setWebhook`, opts);
    return res;
  }

  async sendMessage(chat_id, text, opts) {
    const data = {
      chat_id,
      text,
      ...opts
    }
    const requestOptions = {
      method: 'POST',
      body: { ...data },
    }
    const res = await fetch(`${this.url}/sendMessage`, requestOptions);
    return res;
  }

  async sendAudio(chat_id, audio, opts) {
    const data = {
      chat_id,
      audio,
      ...opts
    }
    const requestOptions = {
      method: 'POST',
      body: { ...data },
    }
    const res = await fetch(`${this.url}/sendAudio`, requestOptions);
    return res;
  }

  async sendVoice(chat_id, voice, opts) {
    const data = {
      chat_id,
      voice,
      ...opts
    }
    const requestOptions = {
      method: 'POST',
      body: { ...data },
    }
    const res = await fetch(`${this.url}/sendVoice`, requestOptions);
    return res;
  }

  async sendFormData(method, form) {
    const requestOptions = {
      method: 'POST',
      body: form,
    }
    const res = await fetch(`${this.url}/${method}`, requestOptions);
    return res;
  }

  async updateMessage(method, chat_id, message_id, opts) {
    const data = {
      chat_id,
      message_id,
      ...opts
    }
    const requestOptions = {
      method: 'POST',
      body: data,
    }
    const res = await fetch(`${this.url}/${method}`, requestOptions);
    return res;
  }
}

module.exports = Telegram;
